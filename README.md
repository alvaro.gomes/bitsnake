# BitSnake

BitSnake é uma implementação do famoso jogo de Snake que marcou várias
gerações. Essa implementação é baseada no script game.js, que é uma tentativa
de criação de engine em JavaScript.

Embora incrívelmente básico e faltando gráficos avançados ainda deve ser muito
divertido e nostálgico para os amantes desse clássico jogo.
