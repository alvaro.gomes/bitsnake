/*
 * This file is part of BitSnake.

 * BitSnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * BitSnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with BitSnake.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

class Snake extends Object {
	constructor(pointID, levelID, stopCallback) {
		super("snake");
		this.points = document.getElementById(pointID);
		this.levels = document.getElementById(levelID);
		this.stopCallback = stopCallback;
	}

	start() {
		this.x = 0;
		this.y = 0;
		this.width = 20;
		this.height = 20;

		this.delayTime = 700;

		this.sumX = 20;
		this.sumY = 0;

		this.points.innerHTML = 0;
		this.levels.innerHTML = 0;

		if (this.first != null) {
			this.first.removeNext();
			this.scene.remove(this.first);
		}
		this.first = new Body(0, 0);
		this.scene.add(this.first);
		this.first.addNext();
		this.first.addNext();
	}

	update() {
		this.x += this.sumX;
		this.y += this.sumY;
		this.first.move(this.x, this.y);

		if (this.x < 0 || this.x >= 320 || this.y < 0
				|| this.y >= 320) {
			this.stopCallback();
		}
	}

	delay() {
		return this.delayTime;
	}

	handleKey(keyCode) {
		if (keyCode == KEY_ARROW_UP && this.sumY == 0) {
			this.sumX = 0;
			this.sumY = -20;
		} else if (keyCode == KEY_ARROW_DOWN && this.sumY == 0) {
			this.sumX = 0;
			this.sumY = 20;
		} else if (keyCode == KEY_ARROW_LEFT && this.sumX == 0) {
			this.sumX = -20;
			this.sumY = 0;
		} else if (keyCode == KEY_ARROW_RIGHT && this.sumX == 0) {
			this.sumX = 20;
			this.sumY = 0;
		} else {
			return false;
		}
		return true;
	}

	handleClick(x, y) {
		if (!this.scene.running) {
			this.scene.start();
			return true;
		}

		if (y <= 160 && this.sumY == 0) {
			this.sumX = 0;
			this.sumY = -20;
		} else if (y > 160 && this.sumY == 0) {
			this.sumX = 0;
			this.sumY = 20;
		} else if (x <= 160 && this.sumX == 0) {
			this.sumX = -20;
			this.sumY = 0;
		} else if (x > 160 && this.sumX == 0) {
			this.sumX = 20;
			this.sumY = 0;
		} else {
			return false;
		}
		return true;
	}

	addValueToHTML(element, amount) {
		var current = element.innerHTML;
		current = parseInt(current) + amount;
		element.innerHTML = current.toString();
		return current;
	}

	addLevel() {
		const current = this.addValueToHTML(levels, 1);

		if (current <= 10) {
			this.delayTime -= 50;
		}
	}

	addPoint() {
		const current = this.addValueToHTML(points, 1);

		if (current % 10 == 0) {
			this.addLevel();
		}
	}

	collidesWith(object) {
		if (object.name == "apple") {
			this.addPoint();
			this.first.addNext();
		} else if (object.name == "body" && object != this.first) {
			this.stopCallback();
		}
	}
}
