/*
 * This file is part of BitSnake.

 * BitSnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * BitSnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with BitSnake.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

const KEY_ARROW_UP = '38';
const KEY_ARROW_DOWN = '40';
const KEY_ARROW_LEFT = '37';
const KEY_ARROW_RIGHT = '39';
const KEY_ESC = '39';

class Object {
	constructor(name) {
		this.name = name;
	}

	start() { }

	update() { }

	delay() {
		return 0;
	}

	draw(ctx) { }

	z() {
		return 0;
	}

	handleKey(keyCode) {
		return false;
	}

	handleClick(x, y) {
		return false;
	}

	checkCollition(object) {
		if (this.x <= object.x && this.x + this.width > object.x
				&& this.y <= object.y
				&& this.y + this.height > object.y) {
			return true;
		} else {
			return false;
		}
	}

	collidesWith(object) { }
}

class Scene {
	constructor(canvas, width, height, color) {
		this.canvasElement = document.getElementById(canvas);
		this.canvasElement.width = width;
		this.canvasElement.height = height;

		this.width = width;
		this.height = height;
		this.color = color;

		this.objectList = [];
		this.objectLoop = [];
		this.sleepTime = 1000 / 60; // Padrao 60 FPS
		this.running = false;

		window.addEventListener('keydown', this.handleKey.bind(this));
		this.canvasElement.addEventListener(
				'click', this.handleClick.bind(this));
	}

	handleKey(e) {
		for (var i=0; i<this.objectList.length; i++) {
			if (this.objectList[i].handleKey(e.keyCode)) {
				break;
			}
		}
	}

	handleClick(e) {
		for (var i=0; i<this.objectList.length; i++) {
			if (this.objectList[i].handleClick(
					e.offsetX, e.offsetY)) {
				break;
			}
		}
	}

	draw() {
		const ctx = this.canvasElement.getContext("2d");
		ctx.fillStyle = this.color;
		ctx.fillRect(0, 0, this.width, this.height);
		for (var i=0; i<this.objectList.length; i++) {
			this.objectList[i].draw(ctx);
		}
		if (this.running) {
			setTimeout(this.draw.bind(this), this.sleepTime);
		}
	}

	setFPS(fps) {
		this.sleepTime = 1000 / fps;
	}

	compareObject(objA, objB) {
		return objA.z() - objB.z();
	}

	runUpdate(object) {
		object.update();

		if (this.running && object.delay() > 0) {
			const index = this.objectList.indexOf(object);
			this.objectLoop[index] = setTimeout(
					this.runUpdate.bind(this, object),
					object.delay());
		}

		for (var i=0; i<this.objectList.length; i++) {
			var curr = this.objectList[i];
			if (object != curr && object.checkCollition(curr)) {
				object.collidesWith(curr);
				curr.collidesWith(object);
			}
		}
	}

	add(object) {
		this.objectList.push(object);
		object.scene = this;
		object.start();
		this.objectList.sort(this.compareObject);
		this.draw();
		if (this.running && object.delay() > 0) {
			this.runUpdate(object);
		}
	}

	remove(object) {
		this.objectList.splice(this.objectList.indexOf(object), 1);
		this.draw();
	}

	start() {
		if (this.running) return;

		this.running = true;
		this.draw();
		for (var i=0; i<this.objectList.length; i++) {
			if (this.objectList[i].delay() > 0) {
				this.runUpdate(this.objectList[i]);
			}
		}
	}

	pause() {
		this.running = false;
		for (var i=0; i<this.objectLoop.length; i++) {
			clearTimeout(this.objectLoop[i]);
		}
	}

	stop() {
		this.pause();
		for (var i=0; i<this.objectList.length; i++) {
			this.objectList[i].start();
		}
		this.draw();
	}
}
