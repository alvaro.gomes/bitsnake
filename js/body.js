/*
 * This file is part of BitSnake.

 * BitSnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * BitSnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with BitSnake.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';
class Body extends Object {
	constructor(x, y) {
		super("body");
		this.x = x;
		this.y = y;
		this.next = null;
	}

	start(x, y) {
		this.width = 20;
		this.height = 20;
	}

	draw(ctx) {
		ctx.drawImage(document.getElementById("body"), this.x, this.y);
	}

	z() {
		return 1;
	}

	move(x, y) {
		if (this.next != null) {
			this.next.move(this.x, this.y);
		}

		this.x = x;
		this.y = y;
	}

	addNext() {
		if (this.next == null) {
			this.next = new Body(this.x, this.y);
			this.scene.add(this.next);
		} else {
			this.next.addNext();
		}
	}

	removeNext() {
		if (this.next != null) {
			this.next.removeNext();
			this.scene.remove(this.next);
		}
	}
}
