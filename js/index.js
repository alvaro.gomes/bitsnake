/*
 * This file is part of BitSnake.

 * BitSnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * BitSnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with BitSnake.  If not, see <https://www.gnu.org/licenses/>.
 */

var scene = new Scene('canvas', 320, 320, "#8F8");
var modal = document.getElementById('modal');

scene.add(new Apple());
scene.add(new Snake('points', 'levels', stop));

function startPause() {
	if (scene.running) {
		scene.pause();
	} else {
		scene.start();
	}
}

function stop() {
	scene.pause();
	modal.style.display = 'flex';
}

function restart() {
	modal.style.display = 'none';
	scene.stop();
}