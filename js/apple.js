/*
 * This file is part of BitSnake.

 * BitSnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * BitSnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with BitSnake.  If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';
class Apple extends Object {
	constructor() {
		super("apple");
	}

	start() {
		this.x = Math.floor(Math.random() * 16) * 20;
		this.y = Math.floor(Math.random() * 16) * 20;
		this.width = 20;
		this.height = 20;
	}

	delay() {
		return 100;
	}

	draw(ctx) {
		ctx.drawImage(document.getElementById("apple"), this.x, this.y);
	}

	collidesWith(object) {
		this.x = Math.floor(Math.random() * 16) * 20;
		this.y = Math.floor(Math.random() * 16) * 20;
	}
}
